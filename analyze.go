package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/bmatcuk/doublestar"

	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/walk"
	"gitlab.com/gitlab-org/security-products/analyzers/mobsf/v2/plugin"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	mobsfDefaultAddr   = "http://localhost:8000/api/v1"
	uploadPath         = "/upload"
	scanPath           = "/scan"
	authHeader         = "Authorization"
	contentTypeHeader  = "Content-Type"
	urlEncodedFormType = "application/x-www-form-urlencoded"
	excludeDirs        = "exclude-dirs"
)

var (
	apiKey           = os.Getenv("MOBSF_API_KEY")
	mobsfAddr        = os.Getenv("MOBSF_ADDR")
	wantedExtensions = []string{
		".xml",
		".pbxproj",
		".plist",
		".java",
		".kt",
		".properties",
		".jar",
		".swift",
		".m",
		".h",
		".c",
	}
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringSliceFlag{
			Name:    excludeDirs,
			Usage:   "List of directories to exclude from scan",
			EnvVars: []string{"SAST_MOBSF_EXCLUDE_DIRS"},
		},
	}
}

func getBaseURL() string {
	if mobsfAddr != "" {
		return mobsfAddr
	}

	return mobsfDefaultAddr
}

type scanKind int

const (
	scanKindManifestAndroidStudio scanKind = iota
	scanKindManifestAndroidEclipse
	scanKindManifestIOS
	scanKindBinaryAndroid
	scanKindBinaryIOS
)

type scanJob struct {
	Kind           scanKind
	EntrypointPath string
}

type noSrcFilesFoundError struct {
	message string
}

func (e noSrcFilesFoundError) Error() string {
	return e.message
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	projectRoot := os.Getenv("CI_PROJECT_DIR")

	scanJobs := createScanJobs(projectRoot)

	excludedDirs := c.StringSlice(excludeDirs)
	log.Debugf("Excluded directories: %s", excludedDirs)

	var consolidatedFindings []*Finding
	for _, job := range scanJobs {
		file, uploadFilename, err := createScanPayload(job, excludedDirs)
		if err != nil {
			if _, ok := errors.Unwrap(err).(noSrcFilesFoundError); ok {
				log.Debugf("Directory %s does not contain any source files, skipping.", job.EntrypointPath)
				continue
			}
			return nil, fmt.Errorf("create scan payload: %w", err)
		}

		info, err := upload(file, uploadFilename)
		if err != nil {
			return nil, fmt.Errorf("failed to upload archive to mobsf: %w", err)
		}

		log.Infof("Starting scan. Type: %s, Upload Hash: %s", info.Type, info.Hash)

		scanReport, err := scan(info)
		if err != nil {
			return nil, fmt.Errorf("scan failed: %w", err)
		}

		tmpReport, err := processRawScanReport(job, scanReport, projectRoot)
		if err != nil {
			return nil, fmt.Errorf("process raw scan report: %w", err)
		}

		consolidatedFindings = append(consolidatedFindings, tmpReport.Findings()...)
	}

	b, err := json.Marshal(consolidatedFindings)
	if err != nil {
		return nil, fmt.Errorf("marshal consolidated report: %v", err)
	}
	return io.NopCloser(bytes.NewReader(b)), nil
}

// createScanJobs walks the directory starting at projectRoot to collect entrypoints that are valid scan targets for
// MobSF.
func createScanJobs(projectRoot string) (scanJobs []scanJob) {
	// Collect all possible entrypoints for scanning. This includes manifest files (.xcodeproj, AndroidManifest.xml) and
	// binaries (.ipa, .apk). An individual scan is performed for each entrypoint.
	var matchedPaths []string
	walkFunc := func(path string, info os.FileInfo, err error) error {
		found, err := plugin.Match(path, info)
		if err != nil {
			return err
		}
		if !found {
			return nil
		}

		relPath, _ := filepath.Rel(projectRoot, path)
		matchedPaths = append(matchedPaths, relPath)
		return nil
	}
	if err := walk.Walk(projectRoot, walkFunc, &walk.Options{MaxDepth: 20}); err != nil {
		log.Fatalf("walk project dir to find entrypoints: %s", err)
	}

	// Derive the kind of scan we need to perform for each entrypoint. These correspond to the types declared by MobSF
	// here: https://github.com/MobSF/Mobile-Security-Framework-MobSF/blob/b43b561a659c84913d497cac583a8f2fa4d0b132/mobsf/templates/general/zip.html#L15-L24
	for _, mp := range matchedPaths {
		match, _ := doublestar.Match("**/*/src/main/AndroidManifest.xml", mp)
		if match {
			log.Debugf("Found Android Studio project manifest at %q", filepath.Join(projectRoot, mp))
			// Traverse three levels back.
			pathComponents := strings.Split(filepath.Join(projectRoot, mp), "/")
			scanJobs = append(scanJobs, scanJob{
				Kind:           scanKindManifestAndroidStudio,
				EntrypointPath: "/" + filepath.Join(pathComponents[0:len(pathComponents)-3]...), // Absolute path from system root.
			})
			continue
		}

		if mp == "AndroidManifest.xml" {
			log.Debugf("Found Eclipse project manifest at %q", filepath.Join(projectRoot, mp))
			scanJobs = append(scanJobs, scanJob{
				Kind:           scanKindManifestAndroidEclipse,
				EntrypointPath: filepath.Dir(filepath.Join(projectRoot, mp)),
			})
			continue
		}

		if filepath.Ext(mp) == ".xcodeproj" {
			log.Debugf("Found XCode project manifest at %q", filepath.Join(projectRoot, mp))
			scanJobs = append(scanJobs, scanJob{
				Kind:           scanKindManifestIOS,
				EntrypointPath: filepath.Dir(filepath.Join(projectRoot, mp)),
			})
			continue
		}

		if filepath.Ext(mp) == ".ipa" {
			log.Debugf("Found iOS binary at %q", filepath.Join(projectRoot, mp))
			scanJobs = append(scanJobs, scanJob{
				Kind:           scanKindBinaryIOS,
				EntrypointPath: filepath.Join(projectRoot, mp),
			})
			continue
		}

		if filepath.Ext(mp) == ".apk" {
			log.Debugf("Found Android binary at %q", filepath.Join(projectRoot, mp))
			scanJobs = append(scanJobs, scanJob{
				Kind:           scanKindBinaryAndroid,
				EntrypointPath: filepath.Join(projectRoot, mp),
			})
			continue
		}
	}

	log.Debugf("Found %d entrypoints to scan", len(scanJobs))

	return scanJobs
}

// createScanPayload returns the file (and associated filename) of the payload to be uploaded to MobSF to execute the
// specified job.
func createScanPayload(job scanJob, excludedDirs []string) (file io.Reader, uploadFilename string, err error) {
	switch job.Kind {
	case scanKindManifestAndroidStudio:
		log.Debugf("About to scan an Android Studio project found at %q...", job.EntrypointPath)
		container, err := os.MkdirTemp("", "mobsf-android-studio-project")
		if err := exec.Command("cp", "-r", job.EntrypointPath, container).Run(); err != nil {
			log.Fatal(err)
		}

		// The manifest _must_ be in `app/src/main/AndroidManifest.xml`
		// To ensure this, we copy the source into a temp dir, rename the module's directory to `app`,
		// and zip the temp dir to emulate this situation.
		// The root level of the zip file will contain the `app` folder.
		if filepath.Base(job.EntrypointPath) != "app" {
			if err := exec.Command("mv", filepath.Join(container, filepath.Base(job.EntrypointPath)), filepath.Join(container, "app")).Run(); err != nil {
				log.Fatal(err)
			}
		}

		file, err = createAndroidArchive(container, excludedDirs)
		if err != nil {
			return nil, "", fmt.Errorf("could not create zip archive of project: %w", err)
		}
		uploadFilename = "archive.zip"
	case scanKindManifestAndroidEclipse:
		log.Debugf("About to scan an Android Eclipse project found at %q...", job.EntrypointPath)
		file, err = createAndroidArchive(job.EntrypointPath, excludedDirs)
		if err != nil {
			return nil, "", fmt.Errorf("could not create zip archive of project: %w", err)
		}
		uploadFilename = "archive.zip"
	case scanKindManifestIOS:
		log.Debugf("About to scan an iOS XCode project found at %q...", job.EntrypointPath)
		file, err = createiOSArchive(job.EntrypointPath, excludedDirs)
		if err != nil {
			return nil, "", fmt.Errorf("could not create zip archive of project: %w", err)
		}
		uploadFilename = "archive.zip"
	case scanKindBinaryIOS, scanKindBinaryAndroid:
		log.Debugf("About to scan an Android OR iOS binary found at %q...", job.EntrypointPath)
		file, err = os.Open(job.EntrypointPath)
		if err != nil {
			return nil, "", fmt.Errorf("could not open package file of project: %w", err)
		}
		uploadFilename = filepath.Base(job.EntrypointPath)
	}

	return file, uploadFilename, nil
}

// processRawReport transforms the raw MobSF scan report according to the kind of scan job that was performed.
// Currently, transformation == recomputing the file paths so they're relative to the projectRoot.
func processRawScanReport(job scanJob, report []byte, projectRoot string) (tmpReport Report, err error) {
	if err := json.Unmarshal(report, &tmpReport); err != nil {
		return tmpReport, fmt.Errorf("unmarshal temp report: %v", err)
	}

	// Replace the file paths returned by MobSF with the full paths from the project root.
	for _, f := range tmpReport.Findings() {
		for _, loc := range f.Locations {
			switch job.Kind {
			case scanKindManifestAndroidStudio:
				fullFilename, _ := filepath.Rel(projectRoot, job.EntrypointPath)
				// MobSF removes src/main/java from the path entirely. It's really annoying, but this is a
				// best-effort guess at what the path should be.
				loc.FileName = filepath.Join(fullFilename, "src", "main", "java", loc.FileName)
			case scanKindManifestAndroidEclipse:
				fullFilename, _ := filepath.Rel(projectRoot, job.EntrypointPath)
				// MobSF removes src/ from the path entirely. It's really annoying, but this is a
				// best-effort guess at what the path should be.
				loc.FileName = filepath.Join(fullFilename, "src", loc.FileName)
			case scanKindManifestIOS:
				fullFilename, _ := filepath.Rel(projectRoot, job.EntrypointPath)
				// No special handling for XCode projects.
				loc.FileName = filepath.Join(fullFilename, loc.FileName)
			case scanKindBinaryAndroid, scanKindBinaryIOS:
				binaryPath, _ := filepath.Rel(projectRoot, job.EntrypointPath)
				// Replace the faux filenames (i.e. k.java) with the path to the binary instead.
				loc.FileName = binaryPath
			}
		}
	}

	return tmpReport, nil
}

func createAndroidArchive(projectDir string, excludedDirs []string) (io.Reader, error) {
	buff, dirContainsJavaSrcFiles, err := createArchive(projectDir, excludedDirs)

	// we only want to create an archive if the dir contains source files, because
	// an archive without any source files will cause mobsf to throw an error when
	// it tries to read the archive
	if !dirContainsJavaSrcFiles {
		return nil, noSrcFilesFoundError{}
	}

	return buff, err
}

func createiOSArchive(projectDir string, excludedDirs []string) (io.Reader, error) {
	buf, _, err := createArchive(projectDir, excludedDirs)

	return buf, err
}

func createArchive(projectDir string, excludedDirs []string) (io.Reader, bool, error) {
	dirContainsJavaSrcFiles := false

	log.Debugf("Creating zip starting in dir: %s", projectDir)
	buff := new(bytes.Buffer)
	w := zip.NewWriter(buff)
	defer w.Close()

	start, _ := os.Getwd()

	if err := os.Chdir(projectDir); err != nil {
		return nil, dirContainsJavaSrcFiles, fmt.Errorf("could not change into directory %q: %w", projectDir, err)
	}

	pwd, _ := os.Getwd()
	log.Debugf("Current working directory: %s", pwd)

	// For now, we will just zip all the the files related to either iOS or Android.
	// At some point, we may wish to collect separate groups of file extensions
	// based on the platform being scanned.
	// To be addressed with https://gitlab.com/gitlab-org/gitlab/-/issues/263474
	err := filepath.Walk(".", fileWalker(w, excludedDirs, &dirContainsJavaSrcFiles))

	if err != nil {
		return nil, dirContainsJavaSrcFiles, fmt.Errorf("error creating zip archive: %w", err)
	}

	// Change back to the start dir before we exit the function.
	if err := os.Chdir(start); err != nil {
		// Shouldn't matter if this errors since the remaining processes (convert)
		// pay no mind to the working directory
	}

	return buff, dirContainsJavaSrcFiles, nil
}

func isWantedExtension(ext string) bool {
	for _, want := range wantedExtensions {
		if ext == want {
			return true
		}
	}

	return false
}

func javaSrcCodeExtension(ext string) bool {
	return ext == ".java" || ext == ".kt"
}

func fileWalker(w *zip.Writer, excludedDirs []string, dirContainsJavaSrcFiles *bool) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Warnf(err.Error())
			return nil
		}

		name := info.Name()
		if strings.HasPrefix(name, ".") && name != "." {
			log.Debugf("%s is a dotfile, skipping", name)
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		if info.IsDir() {
			for _, dir := range excludedDirs {
				if match, _ := filepath.Match(dir, path); match {
					log.Infof("Excluding directory: %s", dir)
					return filepath.SkipDir
				}
			}
			log.Debugf("%s is a directory, skipping", name)
			return nil
		}

		extension := filepath.Ext(name)

		if !isWantedExtension(extension) {
			log.Debugf("%s does not have a wanted file extension, skipping", name)
			return nil
		}

		if javaSrcCodeExtension(extension) {
			*dirContainsJavaSrcFiles = true
		}

		writer, err := w.Create(path)
		if err != nil {
			log.Warnf("Could not create zip writer: %s", err.Error())
			return nil
		}

		f, err := os.Open(filepath.Clean(path))
		if err != nil {
			log.Warnf("Could not open %q: %s", path, err.Error())
			return nil
		}

		if _, err := io.Copy(writer, f); err != nil {
			log.Warnf("Failed to write to archive: %s", err.Error())
			return nil
		}

		log.Debugf("Zipping: %s", path)

		return nil
	}
}

func newRequest(method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, fmt.Errorf("error creating http request: %w", err)
	}
	req.Header.Set(authHeader, apiKey)
	return req, nil
}

func upload(archive io.Reader, filename string) (*scanInfo, error) {
	body, contentType := createMultipart(archive, filename)
	req, err := newRequest(http.MethodPost, getBaseURL()+uploadPath, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set(contentTypeHeader, contentType)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.WithFields(log.Fields{
			"url":   uploadPath,
			"error": err,
		}).Debug("error requesting scan")
		return nil, fmt.Errorf("error requesting scan: %w", err)
	}
	defer resp.Body.Close()
	return parseScanInfo(resp.Body)
}

// scan blocks until the scan is completed and then returns the scan
// report as raw json.
func scan(info *scanInfo) ([]byte, error) {
	body := strings.NewReader(info.ToData().Encode())
	req, err := newRequest(http.MethodPost, getBaseURL()+scanPath, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set(contentTypeHeader, urlEncodedFormType)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("scan request failed: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		b, _ := ioutil.ReadAll(resp.Body)
		return nil, fmt.Errorf("scan responded with unexpected status code (%d): %s", resp.StatusCode, string(b))
	}

	return io.ReadAll(resp.Body)
}

func createMultipart(archive io.Reader, filename string) (*bytes.Buffer, string) {
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="file"; filename="%s"`, filename))
	h.Set("Content-Type", "application/octet-stream")
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	w, _ := writer.CreatePart(h)
	// Should only return an error if the archive has been closed.
	if _, err := io.Copy(w, archive); err != nil {
		log.Warnf("Failed to write %q to archive: %s", w, err.Error())
	}

	if err := writer.Close(); err != nil {
		log.Warnf("Failed to close %q to archive: %s", w, err.Error())
	}

	return body, writer.FormDataContentType()
}

type scanInfo struct {
	Type     string `json:"scan_type"`
	FileName string `json:"file_name"`
	Hash     string `json:"hash"`
}

func parseScanInfo(body io.ReadCloser) (*scanInfo, error) {
	info := new(scanInfo)
	if err := json.NewDecoder(body).Decode(info); err != nil {
		log.WithField("error", err).Debug("failed to unmarshal scan response json")
		return nil, fmt.Errorf("failed to unmarshal scan response json: %w", err)
	}

	return info, nil
}

func (s *scanInfo) ToData() url.Values {
	values := url.Values{}
	values.Set("scan_type", s.Type)
	values.Set("file_name", s.FileName)
	values.Set("hash", s.Hash)
	return values
}
