# MobSF analyzer changelog


## v4.6.5
- Hardcode the secure report version to `15.0.7` (!109)

## v4.6.4
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.2`](https://github.com/urfave/cli/releases/tag/v2.27.2)] (!108)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.2.0` => [`v2.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.4.0)] (!108)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.2` => [`v4.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.4.0)] (!108)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.8` => [`v2.0.9`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.9)] (!108)

## v4.6.3
- upgrade `github.com/stretchr/testify` version [`v1.8.4` => [`v1.9.0`](https://github.com/stretchr/testify/releases/tag/v1.9.0)] (!105)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!105)

## v4.6.2
- Fixed a bug relating to empty entrypoints (!103)

## v4.6.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!102)

## v4.6.0
- Upgrade to MobSF version v3.7.6 (!100)
  - Improved Android Certificate Analysis
  - Updated Android Manifest Analysis Rules
  - Docker base image update to Ubuntu 22.04
  - Migrated from Pip to Poetry for dependency management
  - Migrate from setup.py to use poetry for build and publish
  - Python 3.11 support
  - IOS Swift RulesUpdates
  - Android SCA rules update
  - Entropies scan support for strings
  - Regex Hardening: Fixes possible Regex DoS in rules and MobSF code base

## v4.5.7
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!99)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!99)

## v4.5.6
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!98)

## v4.5.5
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!97)

## v4.5.4
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!96)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!96)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!96)

## v4.5.3
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.4.1`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.4.1) (!95)

## v4.5.2
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.3.8`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.3.8) (!91)

## v4.5.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!90)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.3` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!90)

## v4.5.0
- Add support for identifying the same vulnerability when moved within a file (!86)

## v4.4.0
- Pin to a more recent MobSF base image (!84)
  - Fixes an iOS plist parsing bug (https://github.com/MobSF/Mobile-Security-Framework-MobSF/issues/2144)
  - Improves the detection logic for the Janus vulnerability (https://github.com/MobSF/Mobile-Security-Framework-MobSF/pull/2159)
  - Adds a rule to warn when targeting a potentially vulnerable Android API level (https://github.com/MobSF/Mobile-Security-Framework-MobSF/pull/2114)
  - Adds support for binding the Gunicorn web server to an IPv6 address (https://github.com/MobSF/Mobile-Security-Framework-MobSF/pull/2150)
  - Adds support for scanning ARR and JAR binaries (https://github.com/MobSF/Mobile-Security-Framework-MobSF/pull/2163). This is not yet enabled in this analyser.

## v4.3.3
- Fix zip directory structure for nested Android Studio projects (!80)

## v4.3.2
- upgrade `github.com/urfave/cli/v2` version [`v2.25.5` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!78)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!78)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.3`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.3)] (!78)
- Drop deprecated `vulnerability.Message` field when building report (!78)

## v4.3.1
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (gitlab-org/security-products/analyzers/mobsf!76)
- upgrade `github.com/stretchr/testify` version [`v1.8.2` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (gitlab-org/security-products/analyzers/mobsf!76)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.5`](https://github.com/urfave/cli/releases/tag/v2.25.5)] (gitlab-org/security-products/analyzers/mobsf!76)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (gitlab-org/security-products/analyzers/mobsf!76)

## v4.3.0
- Update `ruleset` module to `v2.0.2` to support loading remote Custom Rulesets (!75)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (!217)

## v4.2.1
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!74)

## v4.2.0
- Add support for scanning multiple projects of any supported type (!73)

## v4.1.0
- Fixes an issue where some Android projects containing multiple `AndroidManifest.xml` files are not scanned properly (!63)

## v4.0.0
- Bump to next major version (!72)
- Upgrade report schema to v15 (!72)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!72)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!72)

## v3.3.4
- upgrade `github.com/stretchr/testify` version [`v1.8.1` => [`v1.8.2`](https://github.com/stretchr/testify/releases/tag/v1.8.2)] (!70)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!70)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!70)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!70)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!70)
- upgrade Go version to v1.19 (!70)

## v3.3.3
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!64)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!64)

## v3.3.2
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!62)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!62)

## v3.3.1
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.1`](https://github.com/stretchr/testify/releases/tag/v1.8.1)] (!59)
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!59)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!59)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.3` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!59)

## v3.3.0
- upgrade [`MobSF`](https://github.com/MobSF/Mobile-Security-Framework-MobSF) version [`3.5.0` => [`3.6.0`](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.6.0)] (!58)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!58)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!58)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!58)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.15.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.3)] (!58)

## v3.2.1
- Update common to `v3.2.1` to fix gotestsum cmd (!57)

## v3.2.0
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!55)
- upgrade `github.com/stretchr/testify` version [`v1.7.0` => [`v1.8.0`](https://github.com/stretchr/testify/releases/tag/v1.8.0)] (!55)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!55)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!55)

## v3.1.2
- Promote `log.Trace` to `log.Debug` for consistency across analyzers (!54)

## v3.1.1
- Upgrade the `command` package for better analyzer messages (!53)

## v3.1.0
- Upgrade core analyzer dependencies (!51)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.0
- Bumping to `v3.0.0` (!49)

## v2.15.1
- Update dependencies - minor/patch version (!46)
  - Update MobSF to version [3.5.0](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.5.0)
    + Improved App Security Scoring Logic
    + Disable CVSSv2 by default
    + Android and iOS SAST rule QA
    + Manifest, Certificate, Transport Security and Network Security rule QA
    + See detailed changes [on GitHub](https://github.com/MobSF/Mobile-Security-Framework-MobSF/compare/v3.4.6...v3.5.0)
  - Update Go dependencies

## v2.15.0
- Update ruleset, report, and command modules to support ruleset overrides (!45)

## v2.14.0
- Update MobSF to version [3.4.6](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.4.6) (!42)
  + Reduce severity of some existing rules
  + Add new Android rules for weak encryption algorithms and CBC Padding Oracle issues
  + Add new Android API call rules
  + Update Android and iOS Static Analysis rules and descriptions
  + Bug fixes and dependency updates
  + See detailed changes [on GitHub](https://github.com/MobSF/Mobile-Security-Framework-MobSF/compare/v3.4.3...v3.4.6)

## v2.13.4
- Update Go dependencies (!43)

## v2.13.3
- Update go to v1.17 (!41)

## v2.13.2
- Update linux OS packages to latest versions (!38)

## v2.13.1
- Improve safe handling of file closures (!36)

## v2.13.0
- Remove Python 3.8 installation (!34)

## v2.12.0
- Move env vars from entrypoint to the Dockerfile (!26)

## v2.11.1
- Update vendor metadata to be GitLab (!29)

## v2.11.0
- Add an option to upload apk/ipa directly (!18)

## v2.10.0
- Update MobSF to version [3.4.3](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.4.3) (!24)
  + Download and perform static analysis of third party apps from VM/AVD
  + Security Hardening
  + Android Dynamic Analysis TLS/SSL Security Tester
  + Refactored Logcat log viewer to show only app specific logs
  + New REST API exposed for TLS/SSL tests

## v2.9.0
- Remove need for external mobsf service (!23)

## v2.8.0
- Update MobSF to version [3.4.0](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.4.0) (!21)
  + Android Hardcoded Secrets False Positive Improvement
  + New Android Crypto Rule
  + Rescan Fail-Safe and Code QA
  + Auto Comment for PR and Issues
  + USE_HOME by default
  + Dynamically Display Config Location
  + Fixed a bug in iOS ATS plist analysis
  + Removed Android Shared Library PIE Check
  + Improved Frida Instrumentation Logic to prevent Frida bypass
  + Fixed a False positive in Android Java Random rule
  + Fixed a bug that caused multiple first time saves of the same scan
  + Fixed Dynamic Analyzer JSON Report REST API bug

## v2.7.0
 - Update report dependency in order to use the report schema version 14.0.0 (!20)

## v2.6.0
- Update MobSF to version [3.3.3](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.3.3) (!19)
  + Android Hardcoded Secrets Improvement
  + iOS IPA binary analysis improvements
  + Improved Android Manifest Analysis
  + Improved Setup
  + Updated to APKiD that is maintained by MobSF Team
  + Static Analysis Rule QA
  + macOS BigSur support
  + Update libsast to skip large files.
  + Improved iOS plist analysis
  + Relaxed Android Source code zip requirements
  + Fixed a bug in Android Shared Library RELRO check
  + Fixed a bug in Windows setup that prevents detection of python version on the first run
  + Fixed a bug in Recent Scan
  + Fixed a bug in root CA naming that prevented traffic interception

## v2.5.0
- Update MobSF to version 3.2.9 (!15)
  + Includes OWASP MSTG mappings in results: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/e5107dc5918a76a0b8fa2c800475ad093746067c
  + Adds Support for Android 10 API 29 for Genymotion: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/000dc362ab5bfc9d0d4d32a61dd093fb6e377be5
  + Fixes a template bug in the File Analysis Section of Android Source: https://github.com/MobSF/Mobile-Security-Framework-MobSF/pull/1640
  + xapk support https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/2648bf918a206066a33b891152fa36eb9c3acb21
  + Updates apktool to 2.5.0 https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/2417f1423f32d2ac3885f4b8ab7abb2688354e81
  + Allows insecure regex matches with kotlin https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/eef603bc4009b88dd183f2ddaed009c112c730c0
  + NIAP Analysis for Android: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/d1f8f87d2d2a21c72223b7b39288a127c924d61f
  + Secret Detection: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/81bd0aabda6f3a865ef30a263a959002745616f7

## v2.4.0
- Add Identifiers to Issue (!12)

## v2.3.0
- Update common to v2.22.0 (!11)
- Update urfave/cli to v2.3.0 (!11)

## v2.2.0
- Enable ruleset disablement (!9)

## v2.1.0
- Map severity `secure` to `severityLevel.Info` (!8)

## v2.0.0
- Finalize v2.0.0 release (!3)
- Mock project into MobSFs expected structure (!1)

## v1.0.0
- Initial beta release. Code provided by H-E-B (!2)
